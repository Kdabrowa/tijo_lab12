package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private int accountNumber = 0;
    private int balance = 0;
    List<Integer> accounts = new ArrayList<>();
    public int createAccount() {
        balance = 0;
        accounts.add(accountNumber);
        return ++accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        int result=0;
            for(Integer account : accounts) {
                if (account == accountNumber) {
                    accounts.remove(accountNumber);
                    result = 0;
                } else {
                    result = -1;
                }
            }
        return result;
    }

    public boolean deposit(int accountNumber, int amount) {
        if (this.accountNumber == accountNumber){
            balance += amount;
            return true;
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        if (this.accountNumber == accountNumber && balance > amount){
            balance -= amount;
            return true;
        }
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        return false;
    }

    public int accountBalance(int accountNumber) {
        if (this.accountNumber == accountNumber){

            return balance;
        }
        return ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        return 0;
    }

}
